import pprint
import json

import requests
from bs4 import BeautifulSoup

skills_url = "https://mhworld.kiranico.com/skill"
r = requests.get(skills_url)

skills = []
set_bonuses = []
skill = {}
in_single_level_skills = False

if r.status_code == 200:
    table_data = [[cell.text for cell in row("td")]
                  for row in BeautifulSoup(r.content, 'html.parser')("tr")]
    for data in table_data:
        if len(data) == 1:
            if data[0] == "\nHasten Recovery\n":
                skills.append(skill)
                in_single_level_skills = True
                skill = {}
                skill['name'] = data[0].replace('\n', '')
        if not in_single_level_skills:
            if len(data) == 1:
                if 'name' in skill:
                    if skill['name'] == 'Resentment':
                        pass
                    else:
                        skills.append(skill)
                    skill = {}
                    skill['name'] = data[0].replace('\n', '')
                else:
                    skill['name'] = data[0].replace('\n', '')
            else:
                skill_level = {
                    'level': data[0].replace(' ', '').replace('\n', '').replace('Lv', ''),
                    'description': data[1]
                }
                if 'levels' in skill:
                    skill['levels'].append(skill_level)
                else:
                    skill['levels'] = []
                    skill['levels'].append(skill_level)
        else:
            if len(data) == 1:
                if 'name' in skill:
                    if 'levels' not in skill:
                        pass
                    else:
                        set_bonuses.append(skill)
                    skill = {}
                    skill['name'] = data[0].replace('\n', '')
                else:
                    skill['name'] = data[0].replace('\n', '')
            else:
                skill_level = {
                    'level': 1,
                    'description': data[1]
                }
                if 'levels' in skill:
                    skill['levels'].append(skill_level)
                else:
                    skill['levels'] = []
                    skill['levels'].append(skill_level)
i = 0
for skill in skills:
    skills[i]['max_level'] = len(skill['levels'])
    i += 1

sorted_skills = sorted(skills, key=lambda x: x['name'])

skill_id = 0
for skill in sorted_skills:
    sorted_skills[skill_id]['id'] = skill_id
    skill_id += 1

# with open('skills.json', 'w') as fp:
    # json.dump(sorted_skills, fp)

decoration_list = {
    'tier_1': [],
    'tier_2': [],
    'tier_3': []
}

with open('decorations.csv') as decorations_csv:
    for line in decorations_csv:
        data = line.split(',')
        decoration = {
            'name': data[0],
            'skill': data[2],
            'ratity': data[3]
        }
        for skill in sorted_skills:
            if skill['name'] == decoration['skill']:
                decoration['skill_index'] = skill['id']
        if str(data[1]) == '1':
            decoration_list['tier_1'].append(decoration)
        elif str(data[1]) == '2':
            decoration_list['tier_2'].append(decoration)
        else:
            decoration_list['tier_3'].append(decoration)

# with open('decorations.json', 'w') as fp:
    # json.dump(decoration_list, fp)

armor_url = 'https://mhworld.kiranico.com/armor'
r = requests.get(armor_url)

armor_sets = []
item_list = {
    "helmets": [],
    "chests": [],
    "arms": [],
    "waists": [],
    "legs": [],
    "untyped": []
}

item_links = []

set_list = BeautifulSoup(r.content, 'html.parser').find('div', id='rank-2')
sets = set_list.find_all('div', class_='card mb-2')
set_id = 0
old_item_list = json.load(open('Equipment.json'))

for armor_set in sets:

    item_rows = armor_set.find_all('td', class_='p-1 align-middle text-center')

    for row in item_rows:
        item_links.append(row.find('a')['href'])

for item_link in item_links:
    data = BeautifulSoup(requests.get(item_link).content, 'html.parser')
    tables = data.find_all('table', class_='table table-sm')
    resistance_card = data.find('h4', class_='card-header', text='Vs. Element').parent
    resistance_rows = resistance_card.find_all('td')
    item = {
        'name': data.find('h1', itemprop="name").text,
        'defense': int(data.find('div', class_='lead').text.replace(' ～ 0', '')),
        'rarity': int(data.find('small', text='Rare').parent.find('div').text),
        'resistances': {
            'fire': int(resistance_rows[1].text.replace('+', '')),
            'water': int(resistance_rows[3].text.replace('+', '')),
            'thunder': int(resistance_rows[5].text.replace('+', '')),
            'ice': int(resistance_rows[7].text.replace('+', '')),
            'dragon': int(resistance_rows[9].text.replace('+', ''))
        },
        'skills': []
    }
    if item['name'] == "Zorah Headgear Apha":
        item['name'] = "Zorah Headgear Alpha"
    skill_card = data.find('h4', class_='card-header', text='Skills').parent
    skill_rows = skill_card.find_all('td')
    i = 0
    skill = {}
    for row in skill_rows:
        if i % 2 == 0:
            skill['name'] = row.text
        else:
            skill['level'] = int(row.text.replace('+', ''))
            list_skill = next(filter(lambda x: x['name'] == skill['name'], sorted_skills), None)
            if list_skill is not None:
                skill['skill_id'] = list_skill['id']
            item['skills'].append(skill)
            skill = {}
        i += 1

    decoration_icons = data.find('small', text='Slots').parent.find_all('i', class_='zmdi')
    i = 0
    for decoration_slot in decoration_icons:
        print(decoration_slot.get('class'))
        if 'zmdi-n-1-square' == decoration_slot.get('class')[1]:
            item['tier_' + str(i + 1) + '_slots'] = 1
        elif 'zmdi-n-2-square' == decoration_slot.get('class')[1]:
            item['tier_' + str(i + 1) + '_slots'] = 2
        elif 'zmdi-n-3-square' == decoration_slot.get('class')[1]:
            item['tier_' + str(i + 1) + '_slots'] = 3
        else:
            item['tier_' + str(i + 1) + '_slots'] = 0
        i += 1
    old_item = next(filter(lambda old_item: old_item['Name'] == item['name'], old_item_list), None)
    if old_item is not None:
        item['type'] = old_item['Type']

        if item['type'] == '1':
            item['id'] = len(item_list['helmets'])
            item_list['helmets'].append(item)
        elif item['type'] == '2':
            item['id'] = len(item_list['chests'])
            item_list['chests'].append(item)
        elif item['type'] == '3':
            item['id'] = len(item_list['arms'])
            item_list['arms'].append(item)
        elif item['type'] == '4':
            item['id'] = len(item_list['waists'])
            item_list['waists'].append(item)
        else:
            item['id'] = len(item_list['legs'])
            item_list['legs'].append(item)
    else:
        item_list['untyped'].append(item)
    print(item)

with open('new_item_list.json', 'w') as fp:
    json.dump(item_list, fp)
